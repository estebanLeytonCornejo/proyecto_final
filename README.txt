PRIMEROS PASOS:
Instrucciones

Seguir los pasos en el orden que está indicado.El documento está pensado para usuarios que no tienen conocimientos informáticos,
por lo que es importante no saltarse ningún punto.

    1. Descargar el servidor Xampp de este enlace : https://www.apachefriends.org/es/download.html
       y seguir los pasos que va pidiendo el programa.
       
    2. Ir a Panel de control/Sistema y seguridad/Herramientas administrativas/ y darle click al ícono de Administrador de internet Informacion Services
       
    3. Se desplegará una ventana nueva.En ella,en la zona superior derecha,clicar sobre Detener 
       
    4. Desplegar Xampp desde donde la aplicación se haya guardado(lo más seguro es que se haya creado un enlace directo en el escritorio).
       
    5. Se abrirá una ventana que pondrá Xampp Control Panel.En ella verás debajo de Module las palabras Apache y MySQL.Justo frente a esas palabras, verás un par de botones Start
	   Debes clicar en los correspondientes botones de Apache y MySQL.Una vez clicados,pondrán Stop.
       
    6. Ir a esta ruta Disco local (C:) / xampp / htdocs  y crear una carpeta con el nombre que desees. 
       
    7. Disponer de la herramienta GIT BASH para descargar el repositorio del proyecto MyMédico.En caso de que no se tenga instalado el programa,
	   se puede descargar de este enlace https://git-scm.com/downloads y descargarlo como un programa cualquiera (Windows).
       
    8. Una vez descargado GitBush ,verás en colores este texto : nombreUsuario@DESKTOP-UBBIF84 MINGW64 ~ (master)
       
    9. Justo debajo escribe cd c:
	
    10. Posteriormente escribe cd xammp/htdocs/nombredelaCarpetaQueCreasteAntes
	
    11. Gracias al paso anterior,te pondrás dentro de la carpeta que has creado.Para importar todo el código del MyMédico dentro de tu 
	    carpeta debes poner este enlace : git clone https://estebanLeytonCornejo@bitbucket.org/estebanLeytonCornejo/proyecto_final.git y darle a intro.
	
    12. En este último paso,podrás desplegar la aplicación con esta ruta desde  la barra del navegador:http://localhost/nombredelaCarpetaQueCreasteAntes/proyecto_final/.

Cargar la BBDD

Este paso es importante,ya que sin la BBDD no podrás guardar los datos que insertes.

1)En la ventana de Xampp Control,debes presionar el botón de Mysql -> Admin.

2)Se abrirá una ventana en tu navegador de PhpMyAdmin.Una vez dentro debes crear la BBDD,es muy sencillo:

	2.1) Verás en la parte superior izquierda un pequeño simbolo de un Base de datos con el nombre de "Nueva".Dale click y 
	     donde pone "Crear base de datos",pega este nombre : id11047638_mymedico 

	2.2) Al darle a "crear" se creará la base de datos con el nombre de id11047638_mymedico.

	2.3) Posiciona el ratón sobre id11047638_mymedico y dale click izquierdo.De esta forma estas preperando la BBDD para introducir datos.

	2.4) Busca en la barra de navegación la pestaña de SQL y clica sobre ella.Se abrirá un cuadro deonde tendrás que pegar el sisgiente código:

			-- phpMyAdmin SQL Dump
			-- version 4.9.1
			-- https://www.phpmyadmin.net/
			--
			-- Servidor: 127.0.0.1
			-- Tiempo de generación: 13-12-2019 a las 19:46:44
			-- Versión del servidor: 10.4.8-MariaDB
			-- Versión de PHP: 7.3.10

			SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
			SET AUTOCOMMIT = 0;
			START TRANSACTION;
			SET time_zone = "+00:00";


			/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
			/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
			/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
			/*!40101 SET NAMES utf8mb4 */;

			--
			-- Base de datos: `id11047638_mymedico`
			--

			-- --------------------------------------------------------

			--
			-- Estructura de tabla para la tabla `user`
			--

			CREATE TABLE `user` (
			  `id_tutor` int(100) NOT NULL,
			  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
			  `pass` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
			  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

			-- --------------------------------------------------------

			--
			-- Estructura de tabla para la tabla `user_h`
			--

			CREATE TABLE `user_h` (
 			 `id_tutor` int(100) NOT NULL,
 			 `nac` date NOT NULL,
 			 `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
 			 `alias` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
 			 `fecha` date NOT NULL,
			  `hora` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
			  `lugar` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
			  `motivo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
			  `observaciones` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
			  `fila` varchar(10) COLLATE utf8_unicode_ci NOT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

			--
			-- Índices para tablas volcadas
			--

			--
			-- Indices de la tabla `user`
			--
			ALTER TABLE `user`
 			 ADD PRIMARY KEY (`id_tutor`);

			--
			-- Indices de la tabla `user_h`
			--
			ALTER TABLE `user_h`
			  ADD KEY `id_tutor` (`id_tutor`);

			--
			-- AUTO_INCREMENT de las tablas volcadas
			--

			--
			-- AUTO_INCREMENT de la tabla `user`
			--
			ALTER TABLE `user`
			  MODIFY `id_tutor` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

			--
			-- Restricciones para tablas volcadas
			--

			--
			-- Filtros para la tabla `user_h`
			--
			ALTER TABLE `user_h`
			  ADD CONSTRAINT `user_h_ibfk_1` FOREIGN KEY (`id_tutor`) REFERENCES `user` (`id_tutor`);
			COMMIT;

			/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
			/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
			/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

	2.5)Presiona sobre "continuar" y ya habrás creado la BBDD.

	
       