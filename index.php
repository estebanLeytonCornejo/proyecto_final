<?php
ob_start();
//header("refresh: 3600;");
include_once("modelo/user.php");
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>index</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="./vista/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="./vista/css/hoja.css"> 	
    </head>
    <body>
        <div id='inicio'> 
            <div id="primera"><p>Mymédico</p></div>
            <div id="segunda"><p>Gestión de datos médicos</p></div>

        </div>

        <div class="container">
            <div class="row">
                <div id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                <div id="cajaLog" class=" col-lg-4 col-md-8 col-sm-8 ">                            
                    <form class="form-horizontal " method="GET" >              
                        <div class="row">                  
                            <div class="col-centrada">                     
                                <div class="form-group" >
                                    <legend>Email</legend>
                                    <input class="form-control" type="email" name = "mail" > 
                                </div>
                                <div class="form-group">
                                    <legend>Contraseña</legend>
                                    <input  class="form-control"  type="password" name = "pass" > 
                                </div>              
                            </div>                
                        </div>
                        <div style ="text-align:center;">
                            <button  class="btn btn-primary "  name='action' value='enviar' >Entrar </button>             
                        </div>
                        <div style ="text-align:center;">
                            <a href="modelo/nuevoUser.php">No tienes cuenta ?</a>
                        </div>
                    </form>
                </div>
                <div id="drc" class="col-lg-4 col-md-2 col-sm-2 "></div>
            </div>
        </div>


        <?php
        include_once('controlador/conexion.php');
        error_reporting(0);

    
        $_SESSION["mail"] = $_GET["mail"];

        $con=new conexion();
        $con->obtieneObjetoUser(); 
        $nuevoUser=$con->obtieneObjetoUser();
        $nuevoUserMail= $nuevoUser->getEmail();
        $nuevoUserPass= $nuevoUser->getPass();

        //Una vez se recoge el objeto desde la BBDD,se almacena en una variable se sesión.De esta forma,el objeto
        //puede ser llamado desdo todos los archivos de la aplicación
        $_SESSION["ob"] =$nuevoUser;



        //una vez se tiene los datos correctos,se hace una comparación del mail y del pass de la BBD con los datos del formulario.Esto pienso que es redundar en las comprobaciones,ya que en obtieneObjeto() se realiza un a comporbación del mail.
        function compara($nuevoUserMail,$nuevoUserPass){
            if(($nuevoUserMail===$_GET["mail"] && $nuevoUserPass===$_GET["pass"])&&($_GET["mail"] !="" || $_GET["pass"] !="" )){
                return true;
            }else{
                return false;
            }
        }

        if(isset($_GET["action"])){
            switch($_GET["action"]){
                case'enviar':
                    //dependiendo de la respuesta (true o false) entrará en el if o else
                    if(compara($nuevoUserMail,$nuevoUserPass)===true){
                       echo '<div class="container">
                            <div class="row">
                                <div  id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                                <div class ="col-lg-4 col-md-8 col-sm-8 correcto_index"> <strong>Conectando con la base de datos... </strong></div>
                                <div  id="drc" class="col-lg-4 col-md-2 col-sm-2"></div>
                            </div>';  
                        header('Refresh:2;url=vista/eleccionDatos.php');
                    }else{
                        echo "<div> datos incorrectos </div>";
                        header('Refresh:1;url=index.php');
                    }
                    break;
            }
        }

        ?>
        <script src ="./vista/js/jquery.js"></script>
        <script src ="./vista/js/bootstrap.min.js"></script>

    </body>
</html>



