<?php
session_start();

class user_h{

    public $id_tutor;
    public $nac;
    public $nombre;
    public $alias;
    public $fecha;
    public $hora;
    public $lugar;
    public $motivo;
    public $observaciones;
    public $fila;

    public function __construct($id_tutor,$nac,$nombre,$alias,$fecha,$hora,$lugar,$motivo,$observaciones,$fila)
    {

        $this->id_tutor = $id_tutor;
        $this->nac = $nac;
        $this->nombre = $nombre;
        $this->alias = $alias;
        $this->fecha = $fecha;
        $this->hora = $hora ;
        $this->lugar = $lugar;
        $this->motivo = $motivo;
        $this->observaciones = $observaciones;
        $this->fila = $fila;

    }


    public function setId_tutor($id_tutor){
        $this->id_tutor = $id_tutor;
    }

    public function getId_tutor(){
        return $this->id_tutor;
    }
    public function setNac($nac){
        $this->nac = $nac;
    }

    public function getNac(){
        return $this->nac;
    }
    public function setNombre($nombre){
        $this->nombre = $nombre;
    }

    public function getNombre(){
        return $this->nombre;
    }
    public function setAlias($alias){
        $this->alias = $alias;
    }

    public function getAlias(){
        return $this->alias;
    }
    public function setFecha($fecha){
        $this->fecha = $fecha;
    }

    public function getFecha(){
        return $this->fecha;
    }
    public function setHora($hora){
        $this->hora = $hora;
    }

    public function getHora(){
        return $this->hora;
    }

    public function setLugar($lugar){
        $this->lugar = $lugar;
    }

    public function getLugar(){
        return $this->lugar;
    }
    public function setMotivo($motivo){
        $this->motivo = $motivo;
    }

    public function getMotivo(){
        return $this->motivo;
    }
    public function setObservaciones($observaciones){
        $this->observaciones = $observaciones;
    }

    public function getObservaciones(){
        return $this->observaciones;
    }
    public function setFila($fila){
        $this->fila = $fila;
    }

    public function getFila(){
        return $this->fila;
    }

    public function InsertarDatosHjxExistente($fecha,$hora,$lugar,$motivo,$obs){
        include_once('user.php');
        include_once('../controlador/conexion.php');
        //include_once('../..index.php');


        //include('user_h.php');
        //con la clase conexion se establece la conexión a la bbdd
        $con = new conexion();
        //$_SESSION["ob"]; 

        $nac=$this->getNac();
        $alias=$this->getAlias();
        $fila;


        $mysqli = $con->bbdd_Conexion();

        $result1 = mysqli_query($mysqli,"SELECT fila from user_h where alias = '$alias'"); 
        while($row = mysqli_fetch_array($result1)){
            //Mediante la variable id_ob ,hago de puente entre para obtener el id
            $fila=$row["fila"];
            $fila++;

        }

        //        $ob = new user($_SESSION["ob"]->getEmail(),$_SESSION["ob"]->getPass(),$_SESSION["ob"]->getNombre(),$_SESSION["ob"]->getId_tutor());
        $ob = $con->obtieneObjetoUser();
        $id_tutor;
        $mail_ob = $ob->getEmail();
        $result2 = mysqli_query($mysqli,"SELECT id_tutor from user where email = '$mail_ob'"); 
        while($row = mysqli_fetch_array($result2)){
            //Mediante la variable id_ob ,hago de puente entre para obtener el id
            $id_tutor = $row["id_tutor"];

        }



        if($fecha==""||$fecha==null){
            echo '<div class="container">
                            <div class="row">
                                <div  id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                                <div class ="col-lg-4 col-md-8 col-sm-8 atencion"> <strong>Completa correctamente la fecha.</strong></div>
                             <div  id="drc" class="col-lg-4 col-md-2 col-sm-2"></div>
                 </div>';              
        }else if($hora==""||$hora==null){
            echo '<div class="container">
                            <div class="row">
                                <div  id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                                <div class ="col-lg-4 col-md-8 col-sm-8 atencion"> <strong>Completa correctamente la hora.</strong></div>
                             <div  id="drc" class="col-lg-4 col-md-2 col-sm-2"></div>
                 </div>';
        }else if($lugar==""||$lugar==null){
            echo '<div class="container">
                            <div class="row">
                                <div  id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                                <div class ="col-lg-4 col-md-8 col-sm-8 atencion"> <strong>Completa correctamente el lugar.</strong></div>
                             <div  id="drc" class="col-lg-4 col-md-2 col-sm-2"></div>
                 </div>';
        }else if($motivo==""||$motivo==null){
            echo '<div class="container">
                            <div class="row">
                                <div  id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                                <div class ="col-lg-4 col-md-8 col-sm-8 atencion"> <strong>Completa correctamente el motivo.</strong></div>
                             <div  id="drc" class="col-lg-4 col-md-2 col-sm-2"></div>
                 </div>';
        
        }else{
           
            //una vez se obtiene el id del tutor se pueden insertar en la tabla user_h los datos para crear y asignar correctamente
            $consulta ="INSERT INTO user_h (id_tutor,nac,nombre,alias,fecha,hora,lugar,motivo,observaciones,fila) values ('$id_tutor','$nac','','$alias','$fecha','$hora','$lugar','$motivo','$obs','$fila');";
            $mysqli->query($consulta); 
             echo'<div class="container">
                            <div class="row">
                                <div  id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                                <div class ="col-lg-4 col-md-8 col-sm-8 correcto"><strong>Datos añadidos correctamente</strong></div>
                             <div  id="drc" class="col-lg-4 col-md-2 col-sm-2"></div>
                 </div>'; 

        }

    }


    public function modDatosHjxExistente($fecha,$hora,$lugar,$motivo,$observaciones){

        include('../controlador/conexion.php');
        //con la clase conexion se establece la conexión a la bbdd
        $con = new conexion();

        $alias = $this->getAlias();
        $fecha_ob =$this->getFecha();
        $hora_ob =$this->getHora(); 
        $lugar_ob =$this->getLugar(); 
        $motivo_ob =$this->getMotivo(); 
        $observaciones_ob =$this->getObservaciones();

        /*
         Mediante este bloque de if,determino:
            -Si la variable es distinta a null,el get es reemplazado por la nueva variable.
            -En caso contrario(si está vacía),la variable es reemplazada por el get que ya existe,insertando los mismos. 
        */

        if($fecha!=NULL){
            $fecha_ob = $fecha;
        }else{
            $fecha = $fecha_ob;  
        }
        if($hora!=NULL){
            $hora_ob = $hora;  
        }else{
            $hora = $hora_ob;  
        }
        if($lugar!=NULL){
            $lugar_ob = $lugar;  
        }else{
            $lugar = $lugar_ob;  
        }
        if($motivo!=NULL){
            $motivo_ob = $motivo;  
        }else{
            $motivo = $motivo_ob;  
        }
        if($observaciones!=NULL){
            $observaciones_ob = $observaciones;  
        }else{
            $observaciones = $observaciones_ob;  
        }

        //recogo mediante variable de session la fila asignada.De esta forma la busqueda de sql es acertada y modifica sólo los datos
        //de dicha fila
        $fila = $_SESSION['Rfila'];


        $mysqli = $con->bbdd_Conexion();
        $result = mysqli_query($mysqli,"UPDATE user_h SET user_h.fecha='$fecha',user_h.hora = '$hora',user_h.lugar ='$lugar',user_h.motivo='$motivo',user_h.observaciones='$observaciones' WHERE user_h.alias ='$alias' and user_h.fila='$fila'");



    }


}

?>

