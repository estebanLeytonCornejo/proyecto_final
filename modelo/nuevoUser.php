<?php
ob_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Nuevo usuario</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../vista/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../vista/css/hoja.css"> 	
    </head>
    <body>
        <div id='inicio'> 
            <div id="primera"><p>Mymédico</p></div>
            <div id="segunda"><p>Gestión de datos médicos</p></div>

        </div>
        <h1 id='bienvenida'>Nuevo usuario</h1>
        <?php
        error_reporting(0);
  
        function insertarNuevoUser() {
            if (isset($_GET['action']) && $_GET['action'] == "insertarNuevoUser") {
                //se llama al objeto de la clase conexion para realizar la conexión 
                //a la BBDD
                include_once('../controlador/conexion.php');
                $con = new conexion();
                $mysqli = $con->bbdd_Conexion();

                $mail = $_GET["mail"];
                $mailComparar;
                $pass =$_GET["pass"];
                $nombre =$_GET["nombre"];
                $result = mysqli_query($mysqli,"SELECT user.email from user where email = '$mail'"); 

                while($row = mysqli_fetch_array($result)){
                    //Mediante la variable id_ob ,hago de puente entre para obtener el id
                    $mailComparar = $row["email"];
                }
                //Restricciones de mail
                if($mailComparar===$mail){
                    echo '<div class="container">
                            <div class="row">
                                <div  id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                                <div class ="col-lg-4 col-md-8 col-sm-8 atencion"> <strong>Ya existe un usuario con ese mail </strong></div>
                                <div  id="drc" class="col-lg-4 col-md-2 col-sm-2"></div>
                            </div>';
                }else if($mail==="" || $mail==null){
                    echo '<div class="container">
                            <div class="row">
                                <div  id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                                <div class ="col-lg-4 col-md-8 col-sm-8 atencion"> <strong>Completa correctamente el mail </strong></div>
                                <div  id="drc" class="col-lg-4 col-md-2 col-sm-2"></div>
                            </div>';
                } else if($pass==""||$pass==null){
                    echo '<div class="container">
                            <div class="row">
                                <div  id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                                <div class ="col-lg-4 col-md-8 col-sm-8 atencion"> <strong>Completa correctamente la contraseña  </strong></div>
                                <div  id="drc" class="col-lg-4 col-md-2 col-sm-2"></div>
                            </div>'; 
                }else if($nombre==""||$nombre==null){
                    echo '<div class="container">
                            <div class="row">
                                <div  id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                                <div class ="col-lg-4 col-md-8 col-sm-8 atencion"> <strong>Completa correctamente el nombre </strong></div>
                                <div  id="drc" class="col-lg-4 col-md-2 col-sm-2"></div>
                            </div>';     
                }else{
                    $consulta2 ="INSERT INTO user (email,pass,nombre)  values ('$mail','$pass','$nombre')";
                    $mysqli->query($consulta2);
                    echo '<div class="container">
                            <div class="row">
                                <div  id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                                <div class ="col-lg-4 col-md-8 col-sm-8 correcto"> <strong>Tus datos se han guardado correctamente </strong></div>
                                <div  id="drc" class="col-lg-4 col-md-2 col-sm-2"></div>
                            </div>';               
                }

            }
        }

        if(isset($_GET["action"])){
            switch($_GET["action"]){
                case 'atras':
                    header('Refresh:1;url=../index.php');
                    ob_end_flush();
                    break;
                    
                case 'salir':
                    header('Refresh:1;url=../index.php');
                    ob_end_flush();
                    break;

            }
        }
        insertarNuevoUser();
        //        formInsertar();
        //        atras();
        ?>
        <div class="container">
            <div class="row">
                <div id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                <div id="cajaLog" class=" col-lg-4 col-md-8 col-sm-8 ">                            
                    <form class="form-horizontal " action ='nuevoUser.php' method="GET" >              
                        <div class="row">                  
                            <div class="col-centrada">                     
                                <div class="form-group" >
                                    <legend>Email</legend>
                                    <input class="form-control" type="mail" name = "mail" > 
                                </div>
                                <div class="form-group">
                                    <legend>Contraseña</legend>
                                    <input class="form-control"  type="password" name = "pass" > 
                                </div> 
                                <div class="form-group" >
                                    <legend>Nombre</legend>
                                    <input class="form-control" type="text" name = "nombre" > 
                                </div>
                            </div>                
                        </div>
                        <div class="centrar">
                            <div class="form-group">
                                <button class='btn btn-success' name ='action' value='guardarNuevoUser'>Enviar</button><br>
                                <input type = 'hidden' name = 'action' value = 'insertarNuevoUser'> <br> 
                            </div>
                            <div class="form-group">
                                <button class='btn btn-primary' name ='action' value='atras'>Atrás</button>
                                <button class='btn btn-danger' name ='action' value='salir'> Salir </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="drc" class="col-lg-4 col-md-2 col-sm-2 "></div>
            </div>    
        </div>



        <script src ="../vista/js/jquery.js"></script>
        <script src ="../vista/js/bootstrap.min.js"></script>

    </body>
</html>