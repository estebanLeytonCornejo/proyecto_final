<?php
class user{
    public $email;
    public $pass;
    public $nombre;
    public $id_tutor;

    public function __construct($email, $pass,$nombre,$id_tutor)
    {
        $this->email = $email;
        $this->pass = $pass;
        $this->nombre = $nombre;
        $this->id_tutor = $id_tutor;
    }

    public function setEmail($email){
        $this->email = $email;
    }

    public function getEmail(){
        return $this->email;
    }
    public function setPass($pass){
        $this->pass = $pass;
    }

    public function getPass(){
        return $this->pass;
    }
    public function setNombre($nombre){
        $this->nombre = $nombre;
    }

    public function getNombre(){
        return $this->nombre;
    }
    public function setId_tutor($id_tutor){
        $this->id_tutor = $id_tutor;
    }

    public function getId_tutor(){
        return $this->id_tutor;
    }

    function obtieneObjeto($mail){
        error_reporting(0);

        //llamada a la clase conexion con include_once,de esta forma evito que se duplique código
        include_once('controlador/conexion.php');
        $con = new conexion();
        $mysqli = $con->bbdd_Conexion();         
        //se recoge el mail introducido por el usuario y se coteja con el existente en la BBDD
        $consulta = "SELECT id_tutor,email,pass,nombre FROM user where email = '$mail'";
        if ($resultado = $mysqli->query($consulta)){
            while ($persona = $resultado->fetch_object()){
                $user = new user($persona->email,$persona->pass,$persona->nombre,$persona->id_tutor);
                return $user;
            }
        }       
    }



    //se reciben los datos desde el form
    /*
        public function altaHija($nac,$nombre,$alias,$fecha,$hora,$lugar,$motivo,$observaciones){
            include('../controlador/conexion.php');
            //con la clase conexion se establece la conexión a la bbdd
            $con = new conexion();
            $id_tutor;
            $aliasComparar;
            $mail_ob = $this->getEmail();
            $mysqli = $con->bbdd_Conexion();


            $result = mysqli_query($mysqli,"SELECT id_tutor from user where email = '$mail_ob'");            
            while($row = mysqli_fetch_array($result)){
                //Mediante la variable id_ob ,hago de puente entre para obtener el id
                $id_tutor = $row["id_tutor"];

            }


            $result = mysqli_query($mysqli,"SELECT user_h.alias from user_h where alias = '$alias'"); 
            while($row = mysqli_fetch_array($result)){
                //Mediante la variable id_ob ,hago de puente entre para obtener el id
                $aliasComparar = $row["alias"];
            }

            if($aliasComparar===$alias){
                echo "YA EXISTE UN USUARIO CON ESE ALIAS";
            }else if($nac==="" || $nac==null){
                echo "El nacimiento debe ser completado correctamente";
            } else if($nombre==""||$nombre==null){
                echo "El nombre debe ser completado correctamente"; 
            }else if($fecha==""||$fecha==null){
                echo "La fecha debe ser completada correctamente";              
            }else if($hora==""||$hora==null){
                echo "La hora debe ser completada correctamente"; 
            }else{

                $consulta ="INSERT INTO user_h (id_tutor,nac,nombre,alias,fecha,hora,lugar,motivo,observaciones,fila) values ('$id_tutor','$nac','$nombre','$alias','$fecha','$hora','$lugar','$motivo','$observaciones','$fila');";
                $mysqli->query($consulta);
                echo"Datos introducidos correctamente";
            }

        }

*/


    public function altaHija($nac,$nombre,$alias,$fecha,$hora,$lugar,$motivo,$observaciones){
        error_reporting(0);
        include('../controlador/conexion.php');
        //con la clase conexion se establece la conexión a la bbdd
        $con = new conexion();
        $id_tutor;
        $aliasComparar;
        $mail_ob = $this->getEmail();
        $mysqli = $con->bbdd_Conexion();

        $result1 = mysqli_query($mysqli,"SELECT id_tutor from user where email = '$mail_ob'"); 
        $fila=0;
        while($row = mysqli_fetch_array($result1)){
            //Mediante la variable id_ob ,hago de puente entre para obtener el id
            $id_tutor = $row["id_tutor"];
            $fila++;

        }

        $result = mysqli_query($mysqli,"SELECT user_h.alias from user_h where alias = '$alias'"); 
        while($row = mysqli_fetch_array($result)){
            //Mediante la variable id_ob ,hago de puente entre para obtener el id
            $aliasComparar = $row["alias"];
        }

        if($aliasComparar===$alias){
            echo '<div class="container">
                            <div class="row">
                                <div  id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                                <div class ="col-lg-4 col-md-8 col-sm-8 atencion"> <strong>'.$alias. ' ya existe como alias,lo sentimos...  </strong></div>
                            <div  id="drc" class="col-lg-4 col-md-2 col-sm-2"></div>
                </div>';  
        }else if($nombre==="" || $nombre==null){
            echo '<div class="container">
                            <div class="row">
                                <div  id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                                <div class ="col-lg-4 col-md-8 col-sm-8 atencion"> <strong>Completa correctamente el nombre.</strong></div>
                            <div  id="drc" class="col-lg-4 col-md-2 col-sm-2"></div>
                 </div>';  
        }else if($alias==="" || $alias==null){
            echo '<div class="container">
                            <div class="row">
                                <div  id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                                <div class ="col-lg-4 col-md-8 col-sm-8 atencion"> <strong>Completa correctamente el alias.</strong></div>
                             <div  id="drc" class="col-lg-4 col-md-2 col-sm-2"></div>
                 </div>'; 
        } else if($nac==""||$nac==null){
            echo '<div class="container">
                            <div class="row">
                                <div  id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                                <div class ="col-lg-4 col-md-8 col-sm-8 atencion"> <strong>Completa correctamente el nacimiento.</strong></div>
                             <div  id="drc" class="col-lg-4 col-md-2 col-sm-2"></div>
                 </div>'; 
        }else if($fecha==""||$fecha==null){
            echo '<div class="container">
                            <div class="row">
                                <div  id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                                <div class ="col-lg-4 col-md-8 col-sm-8 atencion"> <strong>Completa correctamente la fecha.</strong></div>
                             <div  id="drc" class="col-lg-4 col-md-2 col-sm-2"></div>
                 </div>';              
        }else if($hora==""||$hora==null){
            echo '<div class="container">
                            <div class="row">
                                <div  id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                                <div class ="col-lg-4 col-md-8 col-sm-8 atencion"> <strong>Completa correctamente la hora.</strong></div>
                             <div  id="drc" class="col-lg-4 col-md-2 col-sm-2"></div>
                 </div>';  
        }else{

            $consulta ="INSERT INTO user_h (id_tutor,nac,nombre,alias,fecha,hora,lugar,motivo,observaciones,fila) values ('$id_tutor','$nac','$nombre','$alias','$fecha','$hora','$lugar','$motivo','$observaciones','$fila');";
            $mysqli->query($consulta);
            echo'<div class="container">
                            <div class="row">
                                <div  id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                                <div class ="col-lg-4 col-md-8 col-sm-8 correcto"><strong>Bienvenido/a '.$nombre. '!!!  </strong></div>
                             <div  id="drc" class="col-lg-4 col-md-2 col-sm-2"></div>
                 </div>'; 
        }

    }







    //una vez se obtiene el id del tutor se pueden insertar en la tabla user_h los datos para crear y asignar correctamente









    //una vez se obtiene el id del tutor se pueden insertar en la tabla user_h los datos para crear y asignar correctamente



    /*
    public function InsertarDatosHjxExistente($fecha,$hora,$lugar,$motivo,$obs){
        include('../controlador/conexion.php');
        include('user_h.php');
        //con la clase conexion se establece la conexión a la bbdd
        $con = new conexion();
        $_SESSION["ob_h"] =$con->obtieneObjetoHijx();
        $ob_h = new user_h($_SESSION["ob_h"]->id_tutor,$_SESSION["ob_h"]->nac,$_SESSION["ob_h"]->nombre,$_SESSION["ob_h"]->alias,$_SESSION["ob_h"]->fecha,$_SESSION["ob_h"]->hora,$_SESSION["ob_h"]->lugar,$_SESSION["ob_h"]->motivo,$_SESSION["ob_h"]->observaciones,$_SESSION["ob_h"]->fila);
        $nac=$ob_h->getNac();
        $alias=$ob_h->getAlias();
        $fila;


        $mysqli = $con->bbdd_Conexion();

        $result1 = mysqli_query($mysqli,"SELECT fila from user_h where alias = '$alias'"); 
        while($row = mysqli_fetch_array($result1)){
            //Mediante la variable id_ob ,hago de puente entre para obtener el id
            $fila=$row["fila"];
            $fila++;

        }


        $id_tutor;
        $mail_ob = $this->getEmail();
        $result2 = mysqli_query($mysqli,"SELECT id_tutor from user where mail = '$mail_ob'"); 
        while($row = mysqli_fetch_array($result2)){
            //Mediante la variable id_ob ,hago de puente entre para obtener el id
            $id_tutor = $row["id_tutor"];

        }
        //una vez se obtiene el id del tutor se pueden insertar en la tabla user_h los datos para crear y asignar correctamente
        $consulta ="INSERT INTO user_h (id_tutor,nac,nombre,alias,fecha,hora,lugar,motivo,observaciones,fila) values ('$id_tutor','$nac','','$alias','$fecha','$hora','$lugar','$motivo','$obs','$fila');";
        $mysqli->query($consulta); 

    }
    */

    function saberSitienesHijxs(){

        include_once('../controlador/conexion.php');
        include_once('../modelo/user_h.php');

        $nombre;
        $id_tutor =$this->getId_tutor();
        $mail = $this->getEmail();
        $con = new conexion();
        $mysqli = $con->bbdd_Conexion(); 
        $result = mysqli_query($mysqli, "SELECT user_h.nombre FROM user_h INNER JOIN user WHERE user_h.id_tutor = '$id_tutor' and user.mail = '$mail'"); 
        while($row = mysqli_fetch_array($result)){
            $nombre = $row["nombre"];

        }

        if($nombre == ''|| $nombre ==NULL ){
            return false;
        }else{
            return true;
        }
    }
}
?>