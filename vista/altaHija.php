<?php
ob_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Alta hijo/a</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../vista/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../vista/css/hoja.css"> 	
    </head>
    <body>
        <div id='inicio'> 
            <div id="primera"><p>Mymédico</p></div>
            <div id="segunda"><p>Gestión de datos médicos</p></div>

        </div>
        <h1 id='bienvenida'>Dar de alta hij@</h1>
        <?php  
        include_once("../modelo/user.php");
        session_start();

        function insertarNuevHija() {
            if (isset($_GET['action']) && $_GET['action'] == "insertarNuevHija") {
                //se crea la sesión y se almacenan los datos de ella en un obejo user,de esta forma se puede
                //manipular dicho objeto
                //                session_start();
                $ob = new user($_SESSION["ob"]->getEmail(),$_SESSION["ob"]->getPass(),$_SESSION["ob"]->getNombre(),$_SESSION["ob"]->getId_tutor());

                $nac =$_GET["fechaNac"];
                $nombre = $_GET["nombre"];
                $alias = $_GET["alias"];
                $fecha =$_GET["fecha"];
                $hora =$_GET["hora"];
                $lugar =$_GET["lugar"];
                $motivo =$_GET["motivo"];
                $observaciones = $_GET["obs"];
                //función que recoge los get del form y los envia user para posteriormente insertarlos en la BBDD
                $ob->altaHija($nac,$nombre,$alias,$fecha,$hora,$lugar,$motivo,$observaciones);
            }
        }
        insertarNuevHija();


        ?>

        <div class="container">
            <div class="row">
                <div id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                <div id="cajaLog" class=" col-lg-4 col-md-8 col-sm-8 ">                            
                    <form class="form-horizontal " action ='altaHija.php' method="GET" >              
                        <div class="row">                  
                            <div class="col-centrada">                     
                                <div class="form-group" >
                                    <legend>Nombre</legend>
                                    <input class="form-control" type="text" name = "nombre" > 
                                </div>
                                <div class="form-group">
                                    <legend>Alias</legend>
                                    <input class="form-control"  type="text" name = "alias" > 
                                </div> 
                                <div class="form-group" >
                                    <legend>Fecha cita</legend>
                                    <input class="form-control" type="date" name = "fecha" > 
                                </div>
                                <div class="form-group">
                                    <legend>Nacimiento</legend>
                                    <input class="form-control"  type="date" name = "fechaNac" > 
                                </div> 

                                <div class="form-group" >
                                    <legend>Hora cita</legend>
                                    <input class="form-control" type="time" name = "hora" > 
                                </div>
                                <div class="form-group">
                                    <legend>Lugar de la cita</legend>
                                    <input class="form-control"  type="text" name = "lugar" > 
                                </div> 
                                <div class="form-group" >
                                    <legend>Motivo de la cita</legend>
                                    <input class="form-control" type="text" name = "motivo" > 
                                </div>
                                <div class="form-group">
                                    <legend>Observaciones</legend>
                                    <input class="form-control"  type="text" name = "obs" > 
                                </div> 
                            </div>                
                        </div>
                        <div class="centrar">
                            <div class="form-group">
                                <button class='btn btn-success' name ='action' value='guardarNuevaHija'>Enviar</button><br>
                                <input type = 'hidden' name = 'action' value = 'insertarNuevHija'> <br> 
                            </div>
                            <div class="form-group">
                                <button class='btn btn-primary' name ='action' value='atras'>Atrás</button>
                                <button class='btn btn-danger' name ='action' value='salir'> Salir </button>                         
                            </div>
                            <div class="form-group">

                            </div> 
                        </div>
                    </form>


                </div>
                <div id="drc" class="col-lg-4 col-md-2 col-sm-2 "></div>
            </div>    
        </div>


        <?php
        //llamada al objeto user

        function atras(){
            echo "<form id='formSelec'>
                    <button class='botonAtras' name ='action' value='atras'>atrás</button>
                 </form>";
        }
        /*
        function formInsertarNuevaHija(){
            echo 
                "<div id='contenedorFormInsertar'>
                    <div id='formularioInsertar'>
                        <form  action ='altaHija.php' method ='GET'>
                            <input type='text' name='nombre' placeholder ='Nombre'><br>
                            <input type='text' name='alias' placeholder ='Alias'><br>
                            Fecha cita <input type='date' name='fecha' placeholder ='Fecha'><br>
                            Fecha nacimiento<input type='date' name='fechaNac' placeholder ='Fecha'><br>
                            <input type='time' name='hora' placeholder ='Hora'><br>
                            <input type='text' name='lugar' placeholder ='Lugar'><br>
                            <input type='text' name='motivo' placeholder ='Motivo'><br> 
                            <input type='text' name='obs' placeholder ='observaciones'><br> 
                            <button class='botonGuardar' name ='action' value='guardarNuevaHija'>></button><br>
                            <input type = 'hidden' name = 'action' value = 'insertarNuevHija'> <br>           
                        </form>
                    </div>
                </div>";
        }
        */

        if(isset($_GET["action"])){
            switch($_GET["action"]){
                case 'atras':
                    header('Refresh:1;url=eleccionDatos.php');
                    ob_end_flush();
                    break;
                case "salir":
                    header('Refresh:1;url=../index.php');
                    ob_end_flush();
                    break;

            }
        }
        //        var_dump($_SESSION["ob"]);
        //        formInsertarNuevaHija();

        //        atras();


        ?>

        <script src ="../vista/js/jquery.js"></script>
        <script src ="../vista/js/bootstrap.min.js"></script>

    </body>
</html>