<?php
ob_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Datos hija/o</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../vista/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../vista/css/hoja.css"> 	
    </head>
    <body>
        <div id='inicio'> 
            <div id="primera"><p>Mymédico</p></div>
            <div id="segunda"><p>Gestión de datos médicos</p></div>
        </div>
        <h1 id='bienvenida'>Datos hija/o</h1>
        <div class="container">
            <div class="row">
                <div id="izq" class="col-lg-1 col-md-2 col-sm-2"></div>
                <div id="cajaLog" class=" col-lg-10 col-md-8 col-sm-8 "> 
                    <div class="row">

                        <?php
                        error_reporting(0);
                        ob_start();

                        //session_start();

                        include_once('../modelo/user_h.php');
                        include_once('../controlador/conexion.php');
                        $con = new conexion();
                        $mysqli = $con->bbdd_Conexion();
                        $_SESSION["ob_h"] =$con->obtieneObjetoHijx(); 

                        //session_start();
                        $ob_h = new user_h($_SESSION["ob_h"]->id_tutor,$_SESSION["ob_h"]->nac,$_SESSION["ob_h"]->nombre,$_SESSION["ob_h"]->alias,$_SESSION["ob_h"]->fecha,$_SESSION["ob_h"]->hora,$_SESSION["ob_h"]->lugar,$_SESSION["ob_h"]->motivo,$_SESSION["ob_h"]->observaciones,$_SESSION["ob_h"]->fila);

                        //var_dump($_SESSION['ob']);

                        $fechaNac = $ob_h->getNac();
                        $alias = $ob_h->getAlias();
                        //echo"<h2>Datos de ". $ob_h->getAlias()."</h2>";

                        echo "<form class='col-lg-12'>";

                        echo "<div class='form-group margin'> 
  <button class='btn btn-primary'  name ='action' value='insertar'>Añadir datos</button>
  </div>";
                        $result = mysqli_query($mysqli, "SELECT * FROM user_h where user_h.nac='$fechaNac' and user_h.alias ='$alias'");
                        $contador = 0;
                        while($row = mysqli_fetch_array($result)){

                            //Con la variable $fila uno la palabra 'mod'+ el número de la fila.De esta forma obtengo una especie de clave
                            // que me permite acceder al dato especifico que se desea modificar.
                            $mod ="mod".$row["fila"];
                            $borrar = "borrar".$row["fila"];



                            echo '<div>
           <div class="row">
                    <div class="col-lg-6 datosEncabezado" ><strong>Fecha de la cita : </strong></div>
                    <div class="col-lg-6 datosInfo"><i>'.$row["fecha"] .'</i></div>
            </div>  
            <div class="row">
                    <div class="col-lg-6 datosEncabezado" ><strong>Hora de la cita : </strong></div>
                    <div class="col-lg-6 datosInfo"><i>'.$row["hora"] .'</i></div>
            </div>  
         </div> 
         <div>
           <div class="row">
                    <div class="col-lg-6 datosEncabezado"> <strong>Lugar de la cita : </strong></div>
                    <div class="col-lg-6 datosInfo" ><i>'.$row["lugar"] .'</i></div>
            </div>  
         </div>
         <div>
           <div class="row">
                    <div class="col-lg-6 datosEncabezado"><strong>Motivo : </strong></div>
                    <div class="col-lg-6 datosInfo" ><i>'.$row["motivo"] .'</i></div>
            </div>  
         </div> 
         <div>
           <div class="row">
                    <div class="col-lg-6 datosEncabezado"><strong>Observaciones : </strong></div>
                    <div class="col-lg-6 datosInfo"><i>'.$row["observaciones"] .'</i></div>
            </div>  
         </div>';
                            echo"
                  <div class='form-group margin'> 
                  <button class='btn btn-success btn-sm' name ='$mod' value='$mod'>Modificar</button>
                  <button class='btn btn-secondary btn-sm'  name ='$borrar' value='$borrar'>Borrar</button>
                      </div>";


                            if(isset($_GET[$mod])){     
                                if($_GET[$mod] && $row["fila"]){
                                    $_SESSION['mod'] = $mod;
                                    //Una vez se entra en el if,puedo obtener el valor del botón,que corresponde al valor de la fila y asi enviarlo mediante session a 
                                    //la clase user_h,que es donde lo usaré para gestionar la modificación con sql.

                                    //Este trozo de código es importante:
                                    //   -Al usar la varible de sesión que se esncuentra en la parte superior de esta archivo,ocurría que al intentar
                                    //    modificar cualquier dato de la segunda insercción,este era pisado por la primera insercción,impidiendo modificar 
                                    //    correctamente los datos.
                                    //   -Para evitar este error,basicamente piso la varible de session que está arriba del archivo con los datos que se obtienen
                                    //    del bucle while.
                                    $_SESSION['Rfila'] = $row['fila'];
                                    $_SESSION["ob_h"]->fecha=  $row["fecha"];
                                    $_SESSION["ob_h"]->lugar = $row["lugar"];
                                    $_SESSION["ob_h"]->hora = $row["hora"];
                                    $_SESSION["ob_h"]->motivo = $row["motivo"];
                                    $_SESSION["ob_h"]->observaciones = $row["observaciones"];
                                    $_SESSION["ob_h"]->fila = $_SESSION['Rfila'];
                                    $ob_h = new user_h($_SESSION["ob_h"]->id_tutor,$_SESSION["ob_h"]->nac,$_SESSION["ob_h"]->nombre,$_SESSION["ob_h"]->alias,$_SESSION["ob_h"]->fecha,$_SESSION["ob_h"]->hora,$_SESSION["ob_h"]->lugar,$_SESSION["ob_h"]->motivo,$_SESSION["ob_h"]->observaciones,$_SESSION["ob_h"]->fila);
                                    header('Refresh:1;url=modificarDatosHijxExistente.php');

                                }

                            }


                            if(isset($_GET[$borrar])){

                                if(($_GET[$borrar] && $row["fila"])){
                                    //            Aquí hago el mismo procedimiento que antes(meter datos correctos del bucle while )
                                    $_SESSION['Rfila'] = $row['fila'];
                                    $_SESSION["ob_h"]->fecha=  $row["fecha"];
                                    $_SESSION["ob_h"]->lugar = $row["lugar"];
                                    $_SESSION["ob_h"]->hora = $row["hora"];
                                    $_SESSION["ob_h"]->motivo = $row["motivo"];
                                    $_SESSION["ob_h"]->observaciones = $row["observaciones"];
                                    $_SESSION["ob_h"]->fila = $_SESSION['Rfila'];

                                    $ob_h = new user_h($_SESSION["ob_h"]->id_tutor,$_SESSION["ob_h"]->nac,$_SESSION["ob_h"]->nombre,$_SESSION["ob_h"]->alias,$_SESSION["ob_h"]->fecha,$_SESSION["ob_h"]->hora,$_SESSION["ob_h"]->lugar,$_SESSION["ob_h"]->motivo,$_SESSION["ob_h"]->observaciones,$_SESSION["ob_h"]->fila);

                                    //En este parte,preparo el código para decirle que si se borra la primera fila,el dato del nombre se añada a la fila 
                                    //            número 2 antes de borrar la fila 1,ya que si se borra la primera fila,se eliminan todos los datos restantes.
                                    //            Esto ocurre por la siguiente razón:
                                    //                -En la BBDD, la primera insercción de los datos de hijo tiene el dato nombre y hace de conector con los datos que se añadan                        posteriormente de ese mismo hijo
                                    //                -Si se borra la primera fila,las restantes no tienen nombre y el código que llama al objeto no se ejecuta correctamente

                                    $alias = $ob_h->getAlias();     
                                    $filaBorrar = $_SESSION['Rfila'];            
                                    $obs =$_SESSION["ob_h"]->observaciones;
                                    $nombre = $_SESSION["ob_h"]->nombre;

                                    if($row["nombre"]==""){
                                        $consulta = "DELETE from user_h WHERE user_h.alias='$alias' AND user_h.fila='$filaBorrar'";
                                        $mysqli->query($consulta);
                                        header('Refresh:1;url=datosHijx.php');
                                    }else{
                                        $filaOb = $filaBorrar+1 ;
                                        $nombre = $_SESSION["ob_h"]->nombre;
                                        $consulta1 = "UPDATE user_h SET user_h.nombre='$nombre' WHERE user_h.alias ='$alias' and user_h.fila='$filaOb'";
                                        $mysqli->query($consulta1);
                                        $consulta2 = "DELETE from user_h WHERE user_h.alias='$alias' AND user_h.fila='$filaBorrar'";
                                        $mysqli->query($consulta2);
                                        header('Refresh:1;url=datosHijx.php');
                                    }


                                    //                echo  $alias ."<br>";
                                    //                echo  $filaBorrar ."<br>";
                                    //                echo  $obs;
                                }
                            }

                        }

                        // echo "<button class='btn btn-primary' name ='action' value='atras'>Atrás</button> ";                       

                        echo"</form>";





                        if(isset($_GET["action"])){
                            switch($_GET["action"]){

                                case 'insertar':
                                    header('Refresh:1;url=insertarDatosHijxExistente.php');
                                    ob_end_flush();
                                    break;
                                case $fila:
                                    header('Refresh:1;url=modificarDatosHijxExistente.php');
                                    ob_end_flush();
                                    break;
                                case'insertar':
                                    //header('Refresh:1;url=formularioInsertar.php');
                                    ob_end_flush();

                                    break;
                                case 'borrar':
                                    //formBorrar();
                                    header('Refresh:1;url=formularioBorrar.php');
                                    ob_end_flush();
                                    break;
                                case "atras":
                                    header('Refresh:1;url=eleccionDatos.php');
                                    ob_end_flush();
                                    break;
                                case "salir":
                                    header('Refresh:1;url=../index.php');
                                    ob_end_flush();
                                    break;
                            }
                        }
                        ?>
                    </div> 
                </div>                  
            </div>
            <div id="drc" class="col-lg-1 col-md-2 col-sm-2 "></div>
        </div>
        <div class="container">
            <form class="margin">                     
                <button class='btn btn-primary' name ='action' value='atras'>Atrás</button>
                <button class='btn btn-danger' name ='action' value='salir'> Salir </button>                     
            </form>  

        </div>





        <script src ="../vista/js/jquery.js"></script>
        <script src ="../vista/js/bootstrap.min.js"></script>

    </body>
</html>       
