<?php
ob_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Tabla datos</title>
        <link rel="stylesheet" type="text/css" href="../vista/css/hoja.css"> 	

    </head>
    <body>
        <div id='inicio'> 
            <div><p>GESTIÓN DE DATOS MÉDICOS</p></div>  

        </div>
        <?php

        function atras(){
            echo "<form id='formSelec'>
                    <button class='botonAtras' name ='action' value='atras'><</button>
                  </form>";
        }
        function mostrar(){
            include("../modelo/user.php");
            session_start();
            $ob = new user($_SESSION["ob"]->getEmail(),$_SESSION["ob"]->getPass(),$_SESSION["ob"]->getNombre(),$_SESSION["ob"]->getId_tutor());
            include_once('../controlador/conexion.php');
            $con = new conexion();
            $mysqli = $con->bbdd_Conexion();
            $id_tutor;
            $mail_ob = $ob->getEmail();
            $result = mysqli_query($mysqli, "SELECT id_tutor from user where mail = '$mail_ob'"); 
            while($row = mysqli_fetch_array($result)){
                //Mediante la variable id_ob ,hago de puente entre para obtener el id
                $id_tutor = $row["id_tutor"];

            }

            echo '<table border="1" id="tabla"><tr>

                    <td class="cabecera"><font face="verdana"><b>Nombre</b></font></td>       
                    <td class="cabecera"><font face="verdana"><b>Fecha</b></font></td>
                    <td class="cabecera"><font face="verdana"><b>Hora</b></font></td>
                    <td class="cabecera"><font face="verdana"><b>Lugar</b></font></td>
                    <td class="cabecera"><font face="verdana"><b>Motivo</b></font></td>
                    <td class="cabecera"><font face="verdana"><b>Observaciones</b></font></td>                
                    </tr>';
            $result = mysqli_query($mysqli, "SELECT user_h.nombre,fecha,hora,lugar,motivo,observaciones FROM user_h INNER join user where user_h.id_tutor = '$id_tutor' and user.mail = '$mail_ob'");
            $numero = 0;
            while($row = mysqli_fetch_array($result)){
                echo "<tr>
                    <td class='casillas'>".$row["nombre"]."</td>
                    <td class='casillas'>".$row["fecha"]."</td>
                    <td class='casillas'>".$row["hora"]."</td>
                    <td class='casillas'>".$row["lugar"]."</td>
                    <td class='casillas'>".$row["motivo"]."</td>
                    <td class='casillas'>".$row["observaciones"]."</td>
                    </tr>";
            }
            echo '</table>';

        }
        if(isset($_GET["action"])){
            switch($_GET["action"]){
                case 'atras':
                    header('Refresh:1;url=/vista/eleccionDatos.php');
                    ob_end_flush();
                    break;
            }
        }
        mostrar();
        atras();
        ?>
    </body>
</html>