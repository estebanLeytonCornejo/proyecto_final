<?php
ob_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Tabla datos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../vista/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../vista/css/hoja.css"> 	
    </head>
    <body>
        <div id='inicio'> 
            <div id="primera"><p>Mymédico</p></div>
            <div id="segunda"><p>Gestión de datos médicos</p></div>

        </div>
        <?php
        include('../controlador/tabla.php');
        error_reporting(0);
        
        $tabla = new tabla();

        $tabla->mostrar();
        if($tabla->mostrar()===false){
            header('Refresh:1;url=datosHijx.php');
        }     
        ?>
        <script src ="../vista/js/jquery.js"></script>
        <script src ="../vista/js/bootstrap.min.js"></script>
    </body>
</html>