<?php
ob_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Modificando datos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../vista/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../vista/css/hoja.css"> 
    </head>
    <body>
        <div id='inicio'> 
            <div id="primera"><p>Mymédico</p></div>
            <div id="segunda"><p>Gestión de datos médicos</p></div>
        </div>
        <h1 id='bienvenida'>Modificando datos...</h1>
        <?php
        //llamada al objeto user
        include_once("../modelo/user_h.php"); 
        function insertarDatos() {
            if (isset($_GET['action']) && $_GET['action'] == "modDatosHjxExistente") {
                //se crea la sesión y se almacenan los datos de ella en un obejo user,de esta forma se puede
                //manipular dicho objeto
                // session_start();
                //$persona->id_tutor,$persona->nac,$persona->nombre,$persona->alias,$persona->fecha,$persona->hora,$persona->lugar,$persona->motivo,$persona->observaciones
                $ob_h = new user_h($_SESSION["ob_h"]->id_tutor,$_SESSION["ob_h"]->nac,$_SESSION["ob_h"]->nombre,$_SESSION["ob_h"]->alias,$_SESSION["ob_h"]->fecha,$_SESSION["ob_h"]->hora,$_SESSION["ob_h"]->lugar,$_SESSION["ob_h"]->motivo,$_SESSION["ob_h"]->observaciones,$_SESSION["ob_h"]->fila);

                $fecha =$_GET["fecha"];
                $hora =$_GET["hora"];
                $lugar =$_GET["lugar"];
                $motivo =$_GET["motivo"];
                $observaciones = $_GET["obs"];
                if($fecha == NULL && $hora == NULL && $lugar == NULL && $motivo == NULL && $observaciones == NULL){
                    echo '<div class="container">
                            <div class="row">
                                <div  id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                                <div class ="col-lg-4 col-md-8 col-sm-8 atencion"> <strong>Vaya,no has modificado nada</strong></div>
                                <div  id="drc" class="col-lg-4 col-md-2 col-sm-2"></div>
                            </div>';
                }else{
                    //función que recoge los get del form y los envia user para posteriormente insertarlos en la BBDD
                    $ob_h->modDatosHjxExistente($fecha,$hora,$lugar,$motivo,$observaciones);

                    echo '<div class="container">
                            <div class="row">
                                <div  id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                                <div class ="col-lg-4 col-md-8 col-sm-8 correcto"> <strong>Datos modificados </strong></div>
                                <div  id="drc" class="col-lg-4 col-md-2 col-sm-2"></div>
                            </div>';


                }

            }
        }
        if(isset($_GET["action"])){
            switch($_GET["action"]){
                case 'atras':

                    header('Refresh:1;url=datosHijx.php');
                    ob_end_flush();
                    break;
                case 'salir':
                    header('Refresh:2;url=../index.php');
                    ob_end_flush();
                    break;

                case 'modDatosHjxExistente':
                    header('Refresh:2;url=datosHijx.php');
                    ob_end_flush();
                    break;

            }
        }

        insertarDatos();
        //        echo "fila : ".$_SESSION['fila']."<br>";
        //        echo "Row fila : ".$_SESSION['Rfila'];

        ?>
        <div class="container">
            <div class="row">
                <div id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                <div id="cajaLog" class=" col-lg-4 col-md-8 col-sm-8 ">                            
                    <form class="form-horizontal " method="GET" >              
                        <div class="row">                  
                            <div class="col-centrada">                     
                                <div class="form-group" >
                                    <legend>Fecha cita</legend>
                                    <input class="form-control" type="date" name = "fecha" > 
                                </div>
                                <div class="form-group" >
                                    <legend>Hora cita</legend>
                                    <input class="form-control" type="time" name = "hora" > 
                                </div>
                                <div class="form-group">
                                    <legend>Lugar de la cita</legend>
                                    <input class="form-control"  type="text" name = "lugar" > 
                                </div> 
                                <div class="form-group" >
                                    <legend>Motivo de la cita</legend>
                                    <input class="form-control" type="text" name = "motivo" > 
                                </div>
                                <div class="form-group">
                                    <legend>Observaciones</legend>
                                    <input class="form-control"  type="text" name = "obs" > 
                                </div> 
                            </div>                
                        </div>
                        <div class="centrar">
                            <div class="form-group">
                                <button class='btn btn-success' name ='action' value='modificarDatosHjxExistente'>Modificar</button><br>
                                <input type = 'hidden' name = 'action' value = 'modDatosHjxExistente'> <br> 
                            </div>
                            <div class="form-group">
                                <button class='btn btn-primary' name ='action' value='atras'>Atrás</button>
                                <button class='btn btn-danger' name ='action' value='salir'> Salir </button>                         
                            </div>
                            <div class="form-group">

                            </div> 
                        </div>
                    </form>


                </div>
                <div id="drc" class="col-lg-4 col-md-2 col-sm-2 "></div>
            </div>    
        </div>
        <script src ="./vista/js/jquery.js"></script>
        <script src ="./vista/js/bootstrap.min.js"></script>

    </body>
</html>