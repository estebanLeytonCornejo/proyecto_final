<?php
ob_start();
include("../modelo/user.php");
session_start();
//header("refresh: 3600;");
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Datos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../vista/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../vista/css/hoja.css"> 	
    </head>
    <body>
        <div id='inicio'> 
            <div id="primera"><p>Mymédico</p></div>
            <div id="segunda"><p>Gestión de datos médicos</p></div>
        </div>

        <?php

        //se recibe el objeto desde sesión
        // 

        $ob = new user($_SESSION["ob"]->getEmail(),$_SESSION["ob"]->getPass(),$_SESSION["ob"]->getNombre(),$_SESSION["ob"]->getId_tutor());
        echo "<h1 id='bienvenida'><strong>Bienvenid@ ".$ob->getNombre()."</strong></h1>";
        ?>
        <div class="container">
            <div class="row">
                <div id="izq" class="col-lg-4 col-md-2 col-sm-2"></div>
                <div id="cajaLog" class=" col-lg-4 col-md-8 col-sm-8 ">                            
                    <form class="form-horizontal " method="GET" >              
                        <div class="row">                  
                            <div class="col-centrada"> 
                                <div class="form-group">
                                    <button class="botonInsertar" id="fotoMostrar"   name ="action" value="alta"></button> 
                                    <p class="centrar">Alta nuevo hijo/a</p>
                                </div>                       
                                <div class="form-group">
                                    <button class="botonMostrar" id="fotoInsertar"   name ="action" value="mostrar"></button>
                                    <p class="centrar">Mostrar citas médicas</p>              
                                </div>              
                            </div>                
                        </div>
                        <div class="centrar">
                            <div class="form-group" >
                                <button  class="btn btn-danger"  name="action" value="salir" >Salir</button> 
                            </div>
                        </div>
                    </form>
                </div>
                <div id="drc" class="col-lg-4 col-md-2 col-sm-2 "></div>
            </div>
        </div>

      

        <?php
        if(isset($_GET["action"])){
            switch($_GET["action"]){

                case 'alta':
                    header('Refresh:1;url=altaHija.php');
                    ob_end_flush();
                    break;
                case'mostrar':
                    header('Refresh:1;url=determinarHijos.php');
                    ob_end_flush();
                    break;        
                case "salir":
                    header('Refresh:1;url=../index.php');
                    ob_end_flush();
                    break;
            }
        }     
        ?>
        <script src ="../vista/js/jquery.js"></script>
        <script src ="../vista/js/bootstrap.min.js"></script>
    </body>
</html>



