<?php
ob_start();
?>
<html>
    <head>
        <title>Botones de selección</title>
        <link rel="stylesheet" type="text/css" href="../css/hoja.css"> 		
    </head>
    <body>
        <div id='inicio'> 
            <div><p>GESTIÓN DE DATOS MÉDICOS</p></div>       
        </div>

        <?php

        echo "<form id='formSelec'>
<button class='botonSelecClau' name ='action' value='claudia'></button>
<button class='botonSelecNoa' name ='action' value='noa'></button>
<button class='botonSalir' name ='action' value='salir'>X</button>
</form>";

        if(isset($_GET["action"])){
            switch($_GET["action"]){
                case "claudia":
                    header('Refresh:1;url=./claudia/datosClau.php');
                    ob_end_flush();
                    break;
                case "noa":
                    header('Refresh:1;url=./noa/datosNoa.php');
                    ob_end_flush();
                    break;
                case "salir":
                    header('Refresh:1;url=index.php');
                    ob_end_flush();
                    break;

            }
        }

        ?>
    </body>
</html>    

